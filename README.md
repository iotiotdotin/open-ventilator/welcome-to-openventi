# Welcome to Openventi

The goal for this Project is to Introduce new Contributers and choose roles for the Contributers.

## Workflow for Introduction and Choosing Roles

1. **Request Access to the Project**. 

![01](/assets/01.jpg)

**Note**: Your access will be granted within 24 hrs

2. **Choose a Role & Introduce yourself**

Goto [Issues](https://gitlab.com/iotiotdotin/open-ventilator/welcome-to-openventi/-/issues) and Choose your Role by clicking on the Issue.

Once Done just introduce yourself, by commenting inside the issue.

